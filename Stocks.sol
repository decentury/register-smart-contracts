pragma solidity ^0.4.15;

contract Stocks {
    struct Holder {
        uint balance;
        bool isFrozen;
    }

    address internal owner = msg.sender;
    uint public currentSupply = 0;
    uint public maxSupply;
    mapping(address => Holder) internal holders;

    modifier onlyOwner() {
    		require(msg.sender == owner);
    		_;
  	}

    function Stocks(uint startSupply) public {
        maxSupply = startSupply;
    }

    function balanceOf(address user) public constant returns(uint) {
        return holders[user].balance;
    }

    function isFrozen(address user) public constant returns(bool) {
        return holders[user].isFrozen;
    }

    function add(address to, uint number) public onlyOwner {
        require(currentSupply + number <= maxSupply);
        currentSupply += number;
        holders[to] = Holder(number, false);
    }

    function transfer(address from, address to, uint number) public onlyOwner {
        Holder fromHolder = holders[from];
        Holder toHolder = holders[to];

        require(fromHolder.balance >= number);

        fromHolder.balance -= number;
        toHolder.balance += number;
    }

}
