pragma solidity ^0.4.15;

import "./Stocks.sol";

contract Registry {
    struct Request {
        address from;
        address to;
        uint price;
        uint companyID;
        uint number;
        bool isApproved;
        bool finished;
    }

    mapping(uint => Stocks) internal stocks;
    mapping(address => uint) internal balances;
    mapping(uint => Request) internal requests;

    uint nextCompanyID;
    uint nextRequestID;

    address internal owner;
    address internal registryHolder;

    event StocksCreated(uint id, address stocksAddress, string name);
    event RequestAdded(uint id, address from, address to, uint price, uint companyID, uint number);
    event RequestApproved(uint id);
    event RequestVerified(uint id);

    modifier onlyOwner() {
    		require(msg.sender == owner);
    		_;
  	}

    modifier onlyRegistryHolder() {
    		require(msg.sender == registryHolder);
    		_;
    }

    function Registry(address holder) public {
        owner = msg.sender;
        //registryHolder = holder;
        registryHolder = msg.sender;
    }

    function createStocks(string name, uint startSupply) public onlyOwner returns(uint) {
        uint id = nextCompanyID;
        stocks[id] = new Stocks(startSupply);
        nextCompanyID++;
        StocksCreated(id, stocks[id], name);
        return id;
    }

    function balanceOf(address user) public constant returns(uint) {
        return balances[user];
    }

    function stocksOf(uint companyID) public constant returns(Stocks) {
        return stocks[companyID];
    }

    function addRequest(address to, uint price, uint companyID, uint number) public returns(uint) {
        uint id = nextRequestID;
        requests[id] = Request(msg.sender, to, price, companyID, number, false, false);
        nextRequestID++;
        RequestAdded(id, msg.sender, to, price, companyID, number);
        return id;
    }

    function approveRequest(uint id) public {
        Request request = requests[id];

        require(msg.sender == request.to);
        require(balanceOf(request.to) >= request.price);

        request.isApproved = true;

        RequestApproved(id);
    }

    function verifyRequest(uint id) public onlyRegistryHolder {
        Request request = requests[id];
        Stocks companyStocks = stocks[request.companyID];

        require(!request.finished);
        require(request.isApproved);
        require(balanceOf(request.to) >= request.price);

        balances[request.from] += request.price;
        balances[request.to] -= request.price;

        companyStocks.transfer(request.from, request.to, request.number);

        request.finished = true;
        RequestVerified(id);
    }

    //Temporary functions

    function addTokens(address user, uint amount) public onlyOwner {
        balances[user] += amount;
    }

    function addStocks(address user, uint companyID, uint number) public onlyOwner {
        stocks[companyID].add(user, number);
    }
}
